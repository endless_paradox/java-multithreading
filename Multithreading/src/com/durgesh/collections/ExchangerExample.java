package com.durgesh.collections;

import java.util.concurrent.Exchanger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * With the help of Exchange -> two threads can exchange objects
 *
 * exchange() -> exchanging objects is done via one of the two exchange() methods
 *
 *  For example: genetic algorithms, training neural networks
 */
class FirstThread implements Runnable {
	private int counter;
	private Exchanger<Integer> exchanger;
	
	/**
	 */
	FirstThread(int counter, Exchanger<Integer> exchanger) {
		this.counter = counter;
		this.exchanger = exchanger;
	}
	
	public FirstThread(Exchanger<Integer> exchanger) {
		this(0, exchanger);
	}
	
	@Override
	public void run() {
		
		while (true) {
			counter++;
			System.out.println("First thread incremented the counter: " + counter);
			try {
				counter = exchanger.exchange(counter);
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public String toString() {
		return "FirstThread[" +
				"counter=" + counter + ", " +
				"exchanger=" + exchanger + ']';
	}
	
}

class SecondThread implements Runnable {
	private int counter;
	private final Exchanger<Integer> exchanger;
	
	/**
	 */
	SecondThread(int counter, Exchanger<Integer> exchanger) {
		this.counter = counter;
		this.exchanger = exchanger;
	}
	
	SecondThread(Exchanger<Integer> exchanger) {
		this(0, exchanger);
	}
	
	@Override
	public void run() {
		
		while (true) {
			counter--;
			System.out.println("First thread decremented the counter: " + counter);
			try {
				counter = exchanger.exchange(counter);
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public String toString() {
		return "FirstThread[" +
				"counter=" + counter + ", " +
				"exchanger=" + exchanger + ']';
	}
	
}

public class ExchangerExample {
	public static void main(String[] args) {
		var exchanger = new Exchanger<Integer>();
		ExecutorService executorService = Executors.newFixedThreadPool(2);
		executorService.execute(new FirstThread(exchanger));
		executorService.execute(new SecondThread(exchanger));
		executorService.shutdown();
	}
}
