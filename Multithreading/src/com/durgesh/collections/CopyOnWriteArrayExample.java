package com.durgesh.collections;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * CopyOnWriteArray creates a copy of list- which is very expensive operation - O(N)
 */
record ReadTask(List<Integer> list) implements Runnable {
	@Override
	public void run() {
		while (true)    {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println(list);
		}
		
	}
}

record WriteTask(List<Integer> list, Random random) implements Runnable {
	
	public WriteTask(List<Integer> list)    {
		this(list, new Random());
	}
	
	@Override
	public void run() {
		while (true)    {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			list.set(random().nextInt(list.size()), random.nextInt(10));
		}
		
	}
}

record ConcurrentArray(List<Integer> list)   {

	public ConcurrentArray()    {
		this(new CopyOnWriteArrayList<>(Arrays.asList(0,0,0,0,0,0,0,0,0,0)));
	}
	
	public void simulate()  {
		ExecutorService executorService = Executors.newFixedThreadPool(4);
		for (int i = 0; i < 3; i++) {
			executorService.execute(new WriteTask(this.list));
		}
		executorService.execute(new ReadTask(this.list));
		executorService.shutdown();
	}
	
}

public class CopyOnWriteArrayExample {
	public static void main(String[] args) {
		// Vector is slow
		var concurrentArray = new ConcurrentArray();
		concurrentArray.simulate();
	}
}
