package com.durgesh.collections;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * This is an unbounded BlockingQueue of objects that implements delayed interface
 *
 *  - DelayQueue keeps the elements internally until a certain delay has expired
 *
 *  - an object can only be taken from the queue when its delay has expired!!!!
 *
 *  We cannot place null items in the queue - The queue is sorted so that the objects at the head
 *  has a delay that has expired for the longest time
 *
 *  if no delay has expired, then there is no head element and poll() will
 *  return null
 *
 *  size() return the count of both expired and non-expired elements
 */
public class DelayQueueExample {
	
	public static void main(String[] args) {
		BlockingQueue<DelayedWorker> queue = new DelayQueue<>();
		try {
			queue.put(new DelayedWorker(1000, "This is the first message..."));
			queue.put(new DelayedWorker(10000, "This is the second message..."));
			queue.put(new DelayedWorker(4000, "This is the third message..."));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		while (!queue.isEmpty())    {
			try {
				System.out.println(queue.take());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
}

record DelayedWorker(long duration, String message) implements Delayed  {
	
	public DelayedWorker(long duration, String message) {
		this.duration = System.currentTimeMillis() + duration;
		this.message = message;
	}
	
	@Override
	public long getDelay(TimeUnit unit) {
		return unit.convert(duration-System.currentTimeMillis(), TimeUnit.MILLISECONDS);
	}
	
	@Override
	public int compareTo(Delayed otherDelayed) {
		if(this.duration < ((DelayedWorker) otherDelayed).duration)
			return -1;
		else if(this.duration > ((DelayedWorker) otherDelayed).duration)
			return 1;
		return 0;
	}
	
	@Override
	public String toString() {
		return this.message;
	}
}
