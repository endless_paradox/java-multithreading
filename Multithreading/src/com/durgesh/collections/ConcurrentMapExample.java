package com.durgesh.collections;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Default segment is of 16 size. it can be changed. We only lock a segment where a thread is working
 */

record FirstWorkerHM(ConcurrentMap<String, Integer> map) implements Runnable    {
	
	@Override
	public void run() {
		try {
			map.put("B",1);
			map.put("H",2);
			Thread.sleep(1000);
			map.put("F",3);
			map.put("A",4);
			Thread.sleep(1000);
			map.put("E",5);
		} catch (InterruptedException e)    {
			e.printStackTrace();
		}
	
	}
}

record SecondWorkerHM(ConcurrentMap<String, Integer> map) implements Runnable    {
	
	@Override
	public void run() {
		try {
			Thread.sleep(5000);
			System.out.println(map.get("A"));
			Thread.sleep(1000);
			System.out.println(map.get("E"));
			System.out.println(map.get("F"));
		} catch (InterruptedException e)    {
			e.printStackTrace();
		}
		
	}
}

public class ConcurrentMapExample {
	public static void main(String[] args) {
		var map = new ConcurrentHashMap<String, Integer>();
		ExecutorService executorService = Executors.newFixedThreadPool(2);
		executorService.execute(new FirstWorkerHM(map));
		executorService.execute(new SecondWorkerHM(map));
		executorService.shutdown();
	}
}
