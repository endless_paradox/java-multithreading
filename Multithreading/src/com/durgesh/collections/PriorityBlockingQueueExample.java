package com.durgesh.collections;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * PriorityBlockingQueue!!!!
 *
 * It implements BlockingQueue interface
 *
 *  - unbounded concurrent queue
 *  - It uses the same ordering rules as the java.util.PriorityQueue class -> have to implement the comparable interface
 *      The comparable interface will determine what will be the order in the queue
 *
 *      the PriorityQueue can be the same compare() == 0 case
 *
 *  - no null items
 */

record FirstWorkerPQ(BlockingQueue<Person> blockingQueue) implements Runnable   {
	
	@Override
	public void run() {
		try {
			blockingQueue.put(new Person(12, "Adam"));
			blockingQueue.put(new Person(45, "Joe"));
			blockingQueue.put(new Person(78, "Daniel"));
			Thread.sleep(1000);
			blockingQueue.put(new Person(32, "Noel"));
			Thread.sleep(1000);
			blockingQueue.put(new Person(34, "Kevin"));
		} catch (InterruptedException e)    {
			e.printStackTrace();
		}
	}
}

record SecondWorkerPQ(BlockingQueue<Person> blockingQueue) implements Runnable   {
	
	@Override
	public void run() {
		try {
			Thread.sleep(5000);
			System.out.println(blockingQueue.take());
			Thread.sleep(1000);
			System.out.println(blockingQueue.take());
			Thread.sleep(1000);
			System.out.println(blockingQueue.take());
			System.out.println(blockingQueue.take());
			System.out.println(blockingQueue.take());
			
		} catch (InterruptedException e)    {
			e.printStackTrace();
		}
	}
}

record Person(int age, String name) implements Comparable<Person> {
	
	@Override
	public int compareTo(Person o) {
		return Integer.compare(this.age, o.age);
	}
}

public class PriorityBlockingQueueExample {
	public static void main(String[] args) {
		BlockingQueue<Person> queue = new PriorityBlockingQueue<>();
		ExecutorService executorService = Executors.newFixedThreadPool(2);
		executorService.execute(new FirstWorkerPQ(queue));
		executorService.execute(new SecondWorkerPQ(queue));
		executorService.shutdown();
	}
}
