package com.durgesh.forkjoin;

import java.util.concurrent.RecursiveAction;

public class SimpleRecursiveAction extends RecursiveAction {
	
	private int simulatedWork;
	
	public SimpleRecursiveAction(int simulatedWork) {
		this.simulatedWork = simulatedWork;
	}
	
	@Override
	protected void compute() {
		if(simulatedWork > 100) {
			System.out.println("Parallel execution and split task..." + simulatedWork);
			SimpleRecursiveAction simpleRecursiveAction = new SimpleRecursiveAction(simulatedWork/2);
			SimpleRecursiveAction simpleRecursiveAction1 = new SimpleRecursiveAction(simulatedWork/2);
			simpleRecursiveAction.fork();
			simpleRecursiveAction1.fork();
		}
		else {
			System.out.println("No need for parallel execution, seq is OK: " + simulatedWork);
		}
	}
}
