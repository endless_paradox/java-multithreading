package com.durgesh.forkjoin;

import java.util.concurrent.ForkJoinPool;

public class RecursiveTaskExample {
	
	public static void main(String[] args) {
		ForkJoinPool pool = new ForkJoinPool(Runtime.getRuntime().availableProcessors());
		SimpleRecursiveTask task = new SimpleRecursiveTask(1200);
		System.out.println(pool.invoke(task));
	}
}
