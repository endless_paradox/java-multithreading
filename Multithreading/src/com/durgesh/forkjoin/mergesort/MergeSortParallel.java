package com.durgesh.forkjoin.mergesort;

import java.util.Arrays;
import java.util.concurrent.RecursiveAction;

public class MergeSortParallel extends RecursiveAction {
	
	private int[] nums;
	
	public MergeSortParallel(int[] nums) {
		this.nums = nums;
	}
	
	@Override
	protected void compute() {
		if(nums.length <=10)    {
			mergeSort(nums);
			return;
		}
		
		int middleIndex = nums.length/2;
		int[] leftSubArray = Arrays.copyOfRange(nums, 0, middleIndex);
		int[] rightSubArray = Arrays.copyOfRange(nums, middleIndex, nums.length);
		MergeSortParallel mergeSortParallel = new MergeSortParallel(leftSubArray);
		MergeSortParallel mergeSortParallel2 = new MergeSortParallel(rightSubArray);
		invokeAll(mergeSortParallel, mergeSortParallel2);
		merge(leftSubArray, rightSubArray, nums);
	}
	
	public void mergeSort(int[] a) {
		if (a.length < 2) {
			return;
		}
		int mid = a.length / 2;
		int[] l = Arrays.copyOfRange(a, 0, mid);
		int[] r = Arrays.copyOfRange(a, mid, a.length);
		mergeSort(l);
		mergeSort(r);
		
		merge(l, r, a);
	}
	
	public void merge(int[] left, int[] right, int[] nums) {
		
		int i = 0, j = 0, k = 0;
		while (i < left.length && j < right.length) {
			if (left[i] <= right[j]) {
				nums[k++] = left[i++];
			}
			else {
				nums[k++] = right[j++];
			}
		}
		while (i < left.length) {
			nums[k++] = left[i++];
		}
		while (j < right.length) {
			nums[k++] = right[j++];
		}
	}
}
