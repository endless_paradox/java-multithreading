package com.durgesh.forkjoin.mergesort;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;

public class App {
	public static void main(String[] args) {
		int[] nums = initializeNums();
		int[] nums2 = Arrays.copyOf(nums, nums.length);
//		int[] nums = {1,5,4,4,-1,0,4,5,7,8,9};
		MergeSortSequential mergeSortSequential = new MergeSortSequential();
		long start = System.currentTimeMillis();
		mergeSortSequential.mergeSort(nums);
		System.out.println("Seq algorithm: " + (System.currentTimeMillis() - start) + "ms");
		ForkJoinPool pool = new ForkJoinPool(Runtime.getRuntime().availableProcessors());
		MergeSortParallel mergeSortParallel = new MergeSortParallel(nums2);
		start = System.currentTimeMillis();
		pool.invoke(mergeSortParallel);
		System.out.println("|| algorithm: " + (System.currentTimeMillis() - start) + "ms");
//		Arrays.stream(nums).forEach(e -> System.out.print(e + " "));
	}
	
	public static int[] initializeNums()    {
		return new Random().ints(100000000, 0, 1000).parallel().toArray();
	}
	
	
}
