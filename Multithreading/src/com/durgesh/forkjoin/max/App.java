package com.durgesh.forkjoin.max;

import java.util.Random;
import java.util.concurrent.ForkJoinPool;

public class App {
	public static int THRESHOLD = 0;
	
	public static void main(String[] args) {
		int[] nums = initializeNums();
		ForkJoinPool pool = new ForkJoinPool(Runtime.getRuntime().availableProcessors());
		THRESHOLD = nums.length / Runtime.getRuntime().availableProcessors();
		SequentialMaxFinding sequentialMaxFinding = new SequentialMaxFinding();
		ParallelMaxFinding parallelMaxFinding = new ParallelMaxFinding(nums, 0, nums.length);
		long start = System.currentTimeMillis();
		System.out.println("Max: " + sequentialMaxFinding.sequentialMaxFind(nums, nums.length));
		System.out.println("Time Taken: " + (System.currentTimeMillis() - start) + "ms");
		start = System.currentTimeMillis();
		System.out.println("Max: " + pool.invoke(parallelMaxFinding));
		System.out.println("Time Taken: " + (System.currentTimeMillis() - start) + "ms");
	}
	
	private static int[] initializeNums() {
		return new Random().ints(400000000, 0, 1000).parallel().toArray();
	}
	
	
}
