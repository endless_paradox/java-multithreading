package com.durgesh.forkjoin.max;

import java.util.concurrent.RecursiveTask;

public class ParallelMaxFinding extends RecursiveTask<Integer> {
	
	private int[] nums;
	private int lowIndex;
	private int highIndex;
	
	public ParallelMaxFinding(int[] nums, int lowIndex, int highIndex) {
		this.nums = nums;
		this.lowIndex = lowIndex;
		this.highIndex = highIndex;
	}
	
	@Override
	protected Integer compute() {
		if(highIndex - lowIndex < App.THRESHOLD)   {
			return sequentialMaxFind();
		}
		else    {
			int middleIndex = lowIndex + (highIndex - lowIndex)/2;
			ParallelMaxFinding parallelMaxFinding = new ParallelMaxFinding(nums, lowIndex, middleIndex);
			ParallelMaxFinding parallelMaxFinding1 = new ParallelMaxFinding(nums, middleIndex + 1, highIndex);
			invokeAll(parallelMaxFinding, parallelMaxFinding1);
			return Math.max(parallelMaxFinding.join(), parallelMaxFinding1.join());
		}
	}
	
	private int sequentialMaxFind()  {
		int max = nums[lowIndex];
		for (int i = lowIndex + 1; i < highIndex; i++) {
			if(nums[i]> max)
				max = nums[i];
		}
		return max;
	}
}
