package com.durgesh.forkjoin;

import java.util.concurrent.ForkJoinPool;

/**
 * fork() asynchronously execute the given task in the pool
 *  we can call this on RecursiveAction or RecursiveTask<T>
 *
 * join() returns the result of the computation when it is done
 *
 * invoke() execute the given task + return its result upon completion
 */
public class RecursiveActionExample {
	
	public static void main(String[] args) {
		ForkJoinPool forkJoinPool = new ForkJoinPool(Runtime.getRuntime().availableProcessors());
		SimpleRecursiveAction simpleRecursiveAction = new SimpleRecursiveAction(1200);
		forkJoinPool.invoke(simpleRecursiveAction);
	}
}
