package com.durgesh.streams;

record Student(String name, boolean local) {
	
	public Student()    {
		this(null, false);
	}
	
}
