package com.durgesh.streams;

import java.util.Arrays;

public class StreamsStringExample {
	public static void main(String[] args) {
		String[] names = {"Adam", "Daniel", "Martha", "Kevin", "Ben", "Joe", "Brad", "Susan"};
		
		Arrays.stream(names).filter(x -> x.startsWith("A")).forEach(System.out::println);
	}
}
