package com.durgesh.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class App {
	public static void main(String[] args) {
		List<Student> students = new ArrayList<>();
		students.add(new Student("Adam", true));
		students.add(new Student("Sue", false));
		students.add(new Student("Kevin", false));
		students.add(new Student("Joe", true));
		students.add(new Student("Daniel", true));
		
		// We can print the students name
//		students.stream().map(Student::name).filter(x -> x.startsWith("D")).forEach(System.out::println);
		
		// We can print the local student
//		students.stream().filter(student -> student.local() == false).forEach(System.out::println);
		
		// We can count the number of local student
//		long count = students.stream().filter(x -> x.local() == true).count();
//		System.out.println(count);
		
		// We can construct single string of the local students
		String locals = students.stream().filter(s -> !s.local()).map(Student::name).collect(Collectors.joining(" "));
		System.out.println(locals);
	}
}
