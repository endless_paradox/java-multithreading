package com.durgesh.streams;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamFilesExample {
	
	public static void main(String[] args) throws IOException {
		String path = "src/names";
		Stream<String> names = Files.lines(Paths.get(path));
		
		List<String> namesList = names.filter(x -> x.startsWith("A")).collect(Collectors.toList());
		namesList.forEach(System.out::println);
	}
}
