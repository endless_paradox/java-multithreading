package com.durgesh.streams;

import java.util.stream.IntStream;

public class PrimeNumberCheck {
	public static void main(String[] args) {
		// sequential stream
		long currentTime = System.currentTimeMillis();
		
		long numberOfPrimes = IntStream.range(2, Integer.MAX_VALUE/100).filter(PrimeNumberCheck::isPrime).count();
		System.out.println("Num of Primes(seq): " + numberOfPrimes);
		System.out.println("Time Taken(seq): " + (System.currentTimeMillis() - currentTime) + "ms");
		
		//parallel
		currentTime = System.currentTimeMillis();
		numberOfPrimes = IntStream.range(2, Integer.MAX_VALUE/100).parallel().filter(PrimeNumberCheck::isPrime).count();
		System.out.println("Num of Primes(parallel): " + numberOfPrimes);
		System.out.println("Time Taken(parallel): " + (System.currentTimeMillis() - currentTime) + "ms");
	}
	
	public static boolean isPrime(long num) {
		if(num<=1)  return false;
		if(num == 2)    return true;
		if(num % 2 == 0)    return false;
		long maxDivisor = (long) Math.sqrt(num);
		for (int i = 3; i < maxDivisor; i +=2 )
			if(num%i == 0)
				return false;
		
		return true;
	}
}
