package com.durgesh.streams;

import java.util.Arrays;

public class StreamsExample {
	
	public static void main(String[] args) {
		int[] nums = {1,5,3,-2,9,12};
		System.out.println(Arrays.stream(nums).sum());
	}
}
