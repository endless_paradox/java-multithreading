package com.durgesh.parallel;

import java.util.Arrays;
import java.util.Random;

public class MergeSortSequential {
	
	private final int[] nums;
	private final int[] tempArray;
	
	public MergeSortSequential(int[] nums) {
		this.nums = nums;
		this.tempArray = new int[nums.length];
	}
	
	public void mergeSort(int low, int high)    {
		if(low < high) {
			int middle = (low + high) / 2;
			mergeSort(low, middle);
			mergeSort(middle + 1, high);
			merge(low, middle, high);
		}
	}
	
	private void merge(int low, int middle, int high) {
		if (high + 1 - low >= 0) System.arraycopy(nums, low, tempArray, low, high + 1 - low);
		
		int i = low;
		int j = middle + 1;
		int k = low;
		
		while ((i <= middle) && (j <= high))    {
			if(tempArray[i] <= tempArray[j])    {
				nums[k] = tempArray[i];
				i++;
			}
			else {
				nums[k] = tempArray[j];
				j++;
			}
			k++;
		}
		while (i <= middle) {
			nums[k] = tempArray[i];
			k++;
			i++;
		}
		while (j <= high) {
			nums[k] = tempArray[j];
			k++;
			j++;
		}
		
	}
	
	public void printArray()    {
		Arrays.stream(this.nums).forEach(e -> System.out.print(e + " "));
	}
	
	public static void main(String[] args) {
		Random random = new Random();
		int[] arr = random.ints(30, -500, 500).toArray();
		
		MergeSortSequential mergeSortSequential = new MergeSortSequential(arr);
		
		mergeSortSequential.mergeSort(0, arr.length - 1);
		mergeSortSequential.printArray();
	}
}
