package com.durgesh.parallel;

import java.util.Random;

public class SequentialSum {
	
	public int sum(int[] nums)  {
		int total = 0;
		for (int e: nums)
			total += e;
		return total;
	}
	
}
