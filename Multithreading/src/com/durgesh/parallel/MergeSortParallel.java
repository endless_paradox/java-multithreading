package com.durgesh.parallel;

import java.util.Arrays;
import java.util.Random;

public class MergeSortParallel {
	
	private final int[] nums;
	private final int[] tempArray;
	
	public MergeSortParallel(int[] nums) {
		this.nums = nums;
		this.tempArray = new int[nums.length];
	}
	
	private Thread mergeSortParallel(int low, int high, int numOfThreads)   {
		return new Thread(() -> parallelMergeSort(low, high, numOfThreads/2));
	}
	
	private void parallelMergeSort(int low, int high, int numOfThreads) {
		if(numOfThreads <= 1)   {
			mergeSort(low, high);
			return;
		}
		int middleIndex = low + (high - low)/2;
		Thread leftSorter = mergeSortParallel(low, middleIndex, numOfThreads);
		Thread rightSorter = mergeSortParallel(middleIndex+1, high, numOfThreads);
		leftSorter.start();
		rightSorter.start();
		
		try {
			leftSorter.join();
			rightSorter.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		merge(low, middleIndex, high);
	}
	
	public void mergeSort(int low, int high)    {
		if(low < high) {
			int middle = (low + high) / 2;
			mergeSort(low, middle);
			mergeSort(middle + 1, high);
			merge(low, middle, high);
		}
	}
	
	private void merge(int low, int middle, int high) {
		if (high + 1 - low >= 0) System.arraycopy(nums, low, tempArray, low, high + 1 - low);
		
		int i = low;
		int j = middle + 1;
		int k = low;
		
		while ((i <= middle) && (j <= high))    {
			if(tempArray[i] <= tempArray[j])    {
				nums[k] = tempArray[i];
				i++;
			}
			else {
				nums[k] = tempArray[j];
				j++;
			}
			k++;
		}
		while (i <= middle) {
			nums[k] = tempArray[i];
			k++;
			i++;
		}
		while (j <= high) {
			nums[k] = tempArray[j];
			k++;
			j++;
		}
		
	}
	
	public static void main(String[] args) {
		int numberOfThreads = Runtime.getRuntime().availableProcessors();
		Random random = new Random();
		int[] arr = random.ints(100000000, 0, 100000).parallel().toArray();
		int[] arr2 = Arrays.copyOf(arr, arr.length);
		
		MergeSortParallel mergeSortParallel = new MergeSortParallel(arr);
		MergeSortSequential mergeSortSequential = new MergeSortSequential(arr2);
		
		System.out.println("No of Threads: " + numberOfThreads);
		long startTime = System.currentTimeMillis();
		mergeSortParallel.parallelMergeSort(0, arr.length - 1, numberOfThreads);
		long endTime = System.currentTimeMillis();
		System.out.println("Time taken for 100 000 000 elements parallel: " +  (endTime - startTime));
		
		startTime = System.currentTimeMillis();
		mergeSortSequential.mergeSort(0, arr.length - 1);
		endTime = System.currentTimeMillis();
		System.out.println("Time taken for 100 000 000 elements sequential: " +  (endTime - startTime));
	}
}
