package com.durgesh.parallel;

import java.util.Random;

public class SumApp {
	
	public static void main(String[] args) {
		Random random = new Random();
		int[] arr = random.ints(1000000000, 0, 2).toArray();
		SequentialSum sequentialSum = new SequentialSum();
		int numberOfThreads = Runtime.getRuntime().availableProcessors();
		ParallelSum parallelSum = new ParallelSum(numberOfThreads);
		long start = System.currentTimeMillis();
		System.out.println(sequentialSum.sum(arr));
		long end = System.currentTimeMillis();
		System.out.println("Sequential time takes: " + (end - start) + "ms");
		start = System.currentTimeMillis();
		System.out.println(parallelSum.sum(arr));
		end = System.currentTimeMillis();
		System.out.println("Parallel time takes: " + (end - start) + "ms");
	}
}
